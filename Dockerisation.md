# Défis : Dockerisation

## Sources

Nous avons utilisés docker-compose pour orchestrer l'ensemble de nos images dockers.

Celui-ci est accessible à cette adresse :
 * https://gitlab.com/cpe-irc-nuit-de-l-info/ndl-back/blob/master/docker/docker-compose.yml

Nous avons également utilisé le docker-file ci-dessous qui nous as permis de mettre en place rapidement un environnement de dev/prod pour nos applications : 
* https://gitlab.com/cpe-irc-nuit-de-l-info/ndl-back/blob/master/Dockerfile


## Services utilisées
* PostgreSQL : la base de données
* Workspace : pour les tâches d'administration de Laravel
* Php-fpm : nécessaire pour le fonctionnement de Laravel
* Nginx : serveur web


## Méthodes
Notre architecture logicielle étant basée sur Laravel, nous avons utilisés des exemples disponibles à cette adresse :
* https://github.com/laradock/laradock
