<?php

use Illuminate\Http\Request;
use App\Http\Resources\Achievement as AchievementResource;
use App\Http\Resources\Chat as ChatResource;
use App\Http\Resources\City as CityResource;
use App\Http\Resources\Collab as CollabResource;
use App\Http\Resources\Event as EventResource;
use App\Http\Resources\Institution as InstitutionResource;
use App\Http\Resources\Interest as InterestResource;
use App\Http\Resources\Process as ProcessResource;
use App\Http\Resources\Stack as StackResource;
use App\Http\Resources\UserLog as UserLogResource;
use App\Http\Resources\User as UserResource;
use App\Models\Chat;
use App\Models\Achievement;
use App\Models\City;
use App\Models\Collab;
use App\Models\Event;
use App\Models\Institution;
use App\Models\Interest;
use App\Models\Process;
use App\Models\Stack;
use App\Models\User;
use App\Models\UserLog;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/achievements', function () {
    return AchievementResource::collection(Achievement::all());
});

Route::middleware('auth:api')->get('/chats', function () {
    return ChatResource::collection(Chat::all());
});

Route::middleware('auth:api')->get('/cities', function () {
    return CityResource::collection(City::all());
});

Route::middleware('auth:api')->get('/collabs', function () {
    return CollabResource::collection(Collab::all());
});

Route::middleware('auth:api')->get('/events', function () {
    return EventResource::collection(Event::all());
});

Route::middleware('auth:api')->get('/institutions', function () {
    return InstitutionResource::collection(Institution::all());
});

Route::middleware('auth:api')->get('/interests', function () {
    return InterestResource::collection(Interest::all());
});

Route::middleware('auth:api')->get('/processes', function () {
    return ProcessResource::collection(Process::all());
});

Route::middleware('auth:api')->get('/stacks', function () {
    return StackResource::collection(Stack::all());
});

Route::middleware('auth:api')->get('/users', function () {
    return UserResource::collection(User::all());
});

Route::middleware('auth:api')->get('/user-logs', function () {
    return UserLogResource::collection(UserLog::all());
});
