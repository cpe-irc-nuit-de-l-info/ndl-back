# Projet : Augementer le pouvoir d'agir des étudiant.e.s en situation de précarité

## Architecture logicielle
**Nous avons décidé de créer :**
* Une application mobile
* Un site Web

L'application mobile est construite à partir de l'application web.

**Les technologies utilisées sont les suivantes :**
* Front : Vue.js
* Backend : Lavarel

## Architecture physique
Nous avons décidé de dockerisé l'ensemble de nos composants, ceux-ci sont listés ci-dessous :
* Nginx : notre serveur web
* PostgreSQL : notre SGBD
* PHP-FMP : nécessaire pour Laravel
* Workspace : nécessaire pour Laravel, permet d'exécuter des commandes d'administration (exemple : migration de BDD entre les versions)



## Les idées principales du projet - La théorie


### Partie 0 : Profil utilisateur
Moyen mis en place :

* La navigation sur le site web / l'application mobile se fait via un compte anonyme ou un compte nommé
* La création du profil inclut des informations permettant de regrouper les utilisateurs par affinitées
* Ces affinitées sont utilisés pour fournir du contenu ciblé à l'utilisateur



### Partie 1 : Foire aux questions et partage d'astuces
Moyen mis en place :

* Un Forum sur lequel les utilisateurs peuvent poser et répondre à des questions
* Les utilisateurs peuvent publier/partager des astuces



### Partie 2 : Axée sur l'humain : Santé, Bien être, Dévelopement Personnel
Moyens mis en place :

* Compteurs :
    * Nombre de fois que l'utilisateur à contacté sa famille/amis
    * Nombre de fois que l'utilisateur à répondu à des questions sur le forum
    * L'activié de l'utilisateur
    * Nombre de publication d'astuces
* System de challenges, badges, récompenses

Le but est de créer des micro récompenses à chaque pas vers "une vie plus saine".
Il nous ai apparu important de rappeler à l'utilisateur qu'il fait des progrès et qu'il ne faut pas baisser les bras.



### Partie 3 : Axée sur l'administratif
Moyens mis en place :

* Mise à disposition de template pour compléter des démarches administratives
* Mise à disposition d'une carte pour visualiser les point d'interêt proche de l'utilisateur : association étudiante, bar, établissement publique, centre d'aides
* En fonction du profil, les démarches que l'utilisateur peut effectuées lui sont proposées



### Partie 4 : Un moyens de communication sur smartphone et en web
Moyens mis en place :

* Les utilisateurs peuvent communiquer avec les personnes de leurs choix
* Les utilisateurs ont également des propositions de personnes avec qui discuter en fonction de son profil (ex: même parcourt scolaire, habite dans la même ville)


### Partie 5 : En cas de détresse
Moyens mis en place :

* Les utilisateurs ont à disposition des numéros d'urgence qu'ils peuvent contacter s'ils rencontre un problème important


### Partie 6 : Partie administrateur
Moyens mis en place :

* Utilisateur de Laraval Nova qui nous à permis de créer un backend pour les administrateurs.
* Le backend administrateur permet : 
    * d'ajouter directectement des informations en bas de données.
    * de mofidier le nom de champ dans la BDD.



## En pratique - Fonctionnalités développées
* Profil : 
* Forum : 
* Compteur : 
* Administration : http://51.83.251.9/nova


## Méthode de travail
* Brainstorming
* Définition des fonctionnalités
* Conception architecture logicielle et physique
* Revus au fur et à mesure de la soirée de l'avancement du projet
* Utilisation de Gitlab pour le versionning et le travail en collaboration sur le projet

