<?php

/** @var Factory $factory */
use App\Models\Process;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Process::class, function (Faker $faker) {
    return [
        'label' => $faker->words(5, true),
        'template' => $faker->randomHtml(2,3),
        'description' => $faker->realText(200, 2),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
