<?php

/** @var Factory $factory */
use App\Models\Achievement;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Achievement::class, function (Faker $faker) {
    return [
        'label' => $faker->words(5, true),
        'points' => $faker->numberBetween(10, 100),
        'icon' => 'fa-play',
        'updated_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
    ];
});
