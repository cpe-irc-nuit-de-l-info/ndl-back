<?php

/** @var Factory $factory */
use App\Models\Interest;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Interest::class, function (Faker $faker) {
    return [
        'label' => $faker->words(3, true),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
