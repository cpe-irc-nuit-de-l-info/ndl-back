<?php

/** @var Factory $factory */
use App\Models\UserLog;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(UserLog::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 30),
        'achievement_id' => $faker->numberBetween(1, 30),
        'label' => $faker->words(4, true),
        'points' => $faker->numberBetween(10, 100),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
