<?php

/** @var Factory $factory */
use App\Models\City;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(City::class, function (Faker $faker) {
    return [
        'label' => $faker->city,
        'updated_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
    ];
});
