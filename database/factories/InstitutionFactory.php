<?php

/** @var Factory $factory */
use App\Models\Institution;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Institution::class, function (Faker $faker) {
    return [
        'label' => $faker->company,
        'updated_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
    ];
});
