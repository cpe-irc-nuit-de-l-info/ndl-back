<?php

/** @var Factory $factory */
use App\Models\Event;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 30),
        'description' => $faker->realText(200, 2),
        'title' => $faker->words(5, true),
        'city_id' => $faker->numberBetween(1, 30),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
