<?php

/** @var Factory $factory */
use App\Models\Stack;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Stack::class, function (Faker $faker) {
    return [
        'poster_id' => $faker->numberBetween(1, 30),
        'question' => $faker->realText(200, 2),
        'answerer_id' => $faker->numberBetween(1, 30),
        'response' => $faker->realText(200, 2),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
