<?php

/** @var Factory $factory */
use App\Models\Chat;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Chat::class, function (Faker $faker) {
    return [
        'sender_id' => $faker->numberBetween(1, 30),
        'receiver_id' => $faker->numberBetween(1, 30),
        'message' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
