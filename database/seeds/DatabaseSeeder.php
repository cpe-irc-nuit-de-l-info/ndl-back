<?php

use App\Models\Achievement;
use App\Models\Chat;
use App\Models\City;
use App\Models\Collab;
use App\Models\Event;
use App\Models\Institution;
use App\Models\Interest;
use App\Models\Process;
use App\Models\Stack;
use App\Models\User;
use App\Models\UserLog;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Achievement::class, 30)->create();
        factory(City::class, 30)->create();
        factory(Institution::class, 30)->create();
        factory(Interest::class, 30)->create();
        factory(User::class, 30)->create()->each(function (User $user) {
            $user->achievements()->attach(Achievement::all()->random(), ['created_at' => now(), 'updated_at' => now()]);
            $user->interests()->attach(Interest::all()->random(), ['created_at' => now(), 'updated_at' => now()]);
        });
        factory(UserLog::class, 30)->create();
        factory(Process::class, 30)->create();
        factory(Collab::class, 30)->create();
        factory(Event::class, 30)->create();
        factory(Chat::class, 30)->create();
        factory(Stack::class, 30)->create();
    }
}
