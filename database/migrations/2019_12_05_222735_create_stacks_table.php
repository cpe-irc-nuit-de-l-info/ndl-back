<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stacks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('poster_id')->unsigned()->nullable();
            $table->text('question');
            $table->bigInteger('answerer_id')->unsigned()->nullable();
            $table->text('response')->nullable();
            $table->timestamps();
            $table->foreign('poster_id')->references('id')->on('users');
            $table->foreign('answerer_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stacks');
    }
}
